package com.cpe.jdtc.audiotool.audio.listener;

import com.cpe.jdtc.audiotool.audio.model.AudioFile;

public interface MyListener {
    public  void onDoneSomething (AudioFile audioFile);
}

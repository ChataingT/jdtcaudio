package com.cpe.jdtc.audiotool.audio.webservice;

import android.util.Log;
import android.util.Xml;

import com.cpe.jdtc.audiotool.audio.model.AudioFile;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class WebService {
    private final String URL = "http://ws.audioscrobbler.com/2.0/";

    Xml xmlResult;
    JSONObject json;
    WebService(){
        json = new JSONObject();
    }

    private InputStream sendRequest(java.net.URL url) throws Exception {

        try {
            // Ouverture de la connexion
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            // Connexion à l'URL
            urlConnection.connect();

            // Si le serveur nous répond avec un code OK
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return urlConnection.getInputStream();
            }
        } catch (Exception e) {
            throw new Exception("");
        }
        return null;
    }

    /*public ArrayList<AudioFile> getAudioFilesFromAPI() {

        try {
            // Envoi de la requête
            InputStream inputStream = sendRequest(new URL(URL));

            // Vérification de l'inputStream
            if(inputStream != null) {
                // Lecture de l'inputStream dans un reader
                InputStreamReader reader = new InputStreamReader(inputStream);

                // Retourne la liste désérialisée par le moteur xml
                //return xmlResult.
                return json.fromJson(reader, new TypeToken<ArrayList<AudioFile>>(){}.getType());
            }

        } catch (Exception e) {
            Log.e("WebService", "Impossible de rapatrier les données :(");
        }
        return null;
    }*/


}

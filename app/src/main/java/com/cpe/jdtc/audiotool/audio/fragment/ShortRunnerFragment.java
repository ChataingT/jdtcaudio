package com.cpe.jdtc.audiotool.audio.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.cpe.jdtc.audiotool.audio.listener.MyButtonListener;
import com.cpe.jdtc.audiotool.audio.model.AudioFile;
import com.cpe.jdtc.audiotool.audio.viewmodel.ShortRunnerViewModel;
import com.cpe.jdtc.audiotool.databinding.ShortRunnerFragmentBinding;
import com.cpe.jdtc.audiotool.R;
import com.cpe.jdtc.audiotool.audio.listener.MyListener;

public class ShortRunnerFragment extends Fragment {

    private MyListener myListener;

    public void setMyListener(MyListener myListener){
        this.myListener = myListener;
    }
    private ShortRunnerViewModel viewModel = new ShortRunnerViewModel();
    private MyButtonListener myButtonListener;
    private AudioFileListFragment audioList = new AudioFileListFragment();
    public void setMyButtonListener(MyButtonListener myListener){
        this.myButtonListener = myListener;
    }

    public void updateAudioDescription (AudioFile audioFile){

        viewModel.setAudioFile(audioFile);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @NonNull ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        ShortRunnerFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.short_runner_fragment, container, false);



        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myButtonListener.onDoneSomething("next");
            }
        });
        binding.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myButtonListener.onDoneSomething("play");
            }
        });
        binding.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myButtonListener.onDoneSomething("previous");
            }
        });
        binding.pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myButtonListener.onDoneSomething("pause");
            }
        });

        binding.setShortRunnerViewModel(viewModel);

        return binding.getRoot();


    }
}

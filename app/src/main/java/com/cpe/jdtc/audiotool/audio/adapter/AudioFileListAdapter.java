package com.cpe.jdtc.audiotool.audio.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.cpe.jdtc.audiotool.R;
import com.cpe.jdtc.audiotool.audio.listener.MyListener;
import com.cpe.jdtc.audiotool.audio.model.AudioFile;
import com.cpe.jdtc.audiotool.audio.viewmodel.AudioFileViewModel;
import com.cpe.jdtc.audiotool.databinding.AudioFileItemBinding;

import java.util.List;

public class AudioFileListAdapter extends RecyclerView.Adapter<AudioFileListAdapter.ViewHolder> {

    private List<AudioFile> audioFileList;
    private MyListener myListener;

    public void setMyListener(MyListener myListener){
        this.myListener = myListener;
    }

    public AudioFileListAdapter(List<AudioFile> fileList) {
        assert fileList != null;
        audioFileList = fileList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AudioFileItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.audio_file_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AudioFile file = audioFileList.get(position);
        /*
        holder.binding.title.setText(file.getTitle());
        holder.binding.artist.setText(file.getArtist());
        holder.binding.album.setText(file.getAlbum());
        holder.binding.duration.setText(file.getDurationText());
        */

        holder.setMyListener(myListener);
        holder.viewModel.setAudioFile(file);
    }

    @Override
    public int getItemCount() {
        return audioFileList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private AudioFileItemBinding binding;
        private AudioFileViewModel viewModel = new AudioFileViewModel();
        private  MyListener myListener;

        public void setMyListener(MyListener myListener){
            this.myListener = myListener;
        }

        ViewHolder(AudioFileItemBinding binding) {
            super(binding.getRoot());
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myListener.onDoneSomething(viewModel.getAudioFile());
                }
            });
            this.binding = binding;
            this.binding.setAudioFileViewModel(viewModel);
        }
    }
}
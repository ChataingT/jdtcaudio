package com.cpe.jdtc.audiotool.audio.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cpe.jdtc.audiotool.R;
import com.cpe.jdtc.audiotool.audio.adapter.AudioFileListAdapter;
import com.cpe.jdtc.audiotool.audio.listener.MyListener;
import com.cpe.jdtc.audiotool.audio.model.AudioFile;
import com.cpe.jdtc.audiotool.databinding.AudioFileListFragmentBinding;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class AudioFileListFragment extends Fragment {
    private MyListener myListener;
    private ArrayList<AudioFile> listAudio = new ArrayList<>();

    public void setMyListener(MyListener myListener){
        this.myListener = myListener;
    }

    @Nullable
    @Override
    public View onCreateView( @NonNull LayoutInflater inflater,
                          @NonNull ViewGroup container,
                          @Nullable Bundle savedInstanceState) {
        AudioFileListFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.audio_file_list_fragment,container,false);
        binding.audioFileList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        Uri uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI; //
        String[] projection = {MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.TITLE,MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM}; //path, title, artist, duration, album
        Cursor cursor = getActivity().getContentResolver().query(uri,projection,null,null,null);

        while (cursor.moveToNext()){
            //create an audiofile element
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            String title= cursor.getString(cursor.getColumnIndex( MediaStore.Audio.Media.TITLE));
            String artist = cursor.getString(cursor.getColumnIndex( MediaStore.Audio.Media.ARTIST));
            String duration = cursor.getString(cursor.getColumnIndex( MediaStore.Audio.Media.DURATION));
            String album =cursor.getString( cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));

            AudioFile audioFile = new AudioFile();
            audioFile.setTitle(title);
            audioFile.setFilePath(path);
            audioFile.setArtist(artist);
            audioFile.setAlbum(album);


            audioFile.setDuration(Integer.parseInt(duration)/1000);
            listAudio.add(audioFile);

        }

        AudioFileListAdapter audioFileListAdapter = new AudioFileListAdapter(listAudio);
        audioFileListAdapter.setMyListener(myListener);
        binding.audioFileList.setAdapter(audioFileListAdapter);

        return binding.getRoot();
    }

    public AudioFile getNextAudio(AudioFile currentAudio) {
        int currentAudioID = listAudio.indexOf(currentAudio);
        AudioFile nextAudioFile = currentAudio;
        if (currentAudioID == (listAudio.size()-1)) {
            nextAudioFile = listAudio.get(0); // If it's the end of the list we give back the first one
        }
        else{
            nextAudioFile = listAudio.get(currentAudioID + 1);
        }
        return nextAudioFile;
    }

    public  AudioFile getPreviousAudio(AudioFile currentAudio) {
        int currentAudioID = listAudio.indexOf(currentAudio);
        AudioFile previousAudioFile = currentAudio;
        if (currentAudioID == 0) {
            previousAudioFile = listAudio.get(listAudio.size() - 1);
        }
        else{
            previousAudioFile = listAudio.get(currentAudioID-1);
        }
        return previousAudioFile;
    }
}

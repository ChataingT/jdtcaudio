package com.cpe.jdtc.audiotool;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;

import android.os.Bundle;
import android.os.IBinder;

import com.cpe.jdtc.audiotool.audio.fragment.AudioFileListFragment;
import com.cpe.jdtc.audiotool.audio.fragment.ShortRunnerFragment;
import com.cpe.jdtc.audiotool.audio.listener.MyButtonListener;
import com.cpe.jdtc.audiotool.audio.listener.MyListener;
import com.cpe.jdtc.audiotool.audio.model.AudioFile;
import com.cpe.jdtc.audiotool.audio.service.PlayerService;
import com.cpe.jdtc.audiotool.databinding.ActivityMainBinding;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    public static final int GLOBAL_EXTERNAL_STORAGE_PERMISSION = 0;
    private ActivityMainBinding binding;
    private AudioFile selectedAudio = new AudioFile();
    private boolean changeSelectedAudio = false;
    private String clickedButton = "init";

    private AudioFileListFragment fragmentList;

    private PlayerService mediaPlayerService;
    private Boolean mediaServiceBound;
    private MyListener audioListener = new MyListener() {
        @Override
        public void onDoneSomething(AudioFile audioFile) {
            selectedAudio = audioFile;

            FragmentManager manager = getSupportFragmentManager();

            ShortRunnerFragment shortRunnerFragment = (ShortRunnerFragment) manager.findFragmentById(R.id.fragment_short_runner);
            shortRunnerFragment.updateAudioDescription(audioFile);
            changeSelectedAudio = true;
            clickedButton = "play";
            playSelected(selectedAudio.getFilePath());
        }
    };

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
                // We've bound to LocalService, cast the IBinder and get LocalService instance
                PlayerService.PlayerBinder binder = (PlayerService.PlayerBinder) service;
                mediaPlayerService = binder.getService();
                mediaServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mediaServiceBound = false;
        }
    };

    private MyButtonListener buttonListener = new MyButtonListener() {
        @Override
        public void onDoneSomething(String buttonSelected) {
            System.out.println(selectedAudio);
            System.out.println("button clicked: " + buttonSelected);
            //if (!buttonSelected.equals(clickedButton) && (!mediaPlayerService.isPlaying())){
                actionManager(buttonSelected);
                //clickedButton = buttonSelected;
            //}
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        this.requestPermission(); //get


        this.showStartup();
    }

    private void requestPermission (){
        // Documentation : https://developer.android.com/training/permissions/requesting.html#java

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        GLOBAL_EXTERNAL_STORAGE_PERMISSION);

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            }
            else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        GLOBAL_EXTERNAL_STORAGE_PERMISSION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case GLOBAL_EXTERNAL_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    break;

                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                } else {
                    System.exit(1);


                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.


                }
            }
            default: {}

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        AudioFileListFragment fragment_list = new AudioFileListFragment();
        fragment_list.setMyListener(this.audioListener);
        fragmentList = fragment_list;
        transaction.replace(R.id.fragment_container, fragment_list);

        ShortRunnerFragment fragment_short_runner = new ShortRunnerFragment();
        fragment_short_runner.setMyButtonListener(this.buttonListener);
        transaction.replace(R.id.fragment_short_runner, fragment_short_runner);

        Intent intentService = new Intent(this, PlayerService.class);
        Boolean test = this.bindService(intentService,connection, BIND_AUTO_CREATE);

        transaction.commit();
    }

    private void playNext(){
        AudioFile nextAudioFile = fragmentList.getNextAudio(selectedAudio);
        changeSelectedAudio = true;
        try {
            mediaPlayerService.play(nextAudioFile.getFilePath());
            selectedAudio = nextAudioFile;
            audioListener.onDoneSomething(selectedAudio);
        }
        catch (IOException e){
            System.out.println(e);
        }
    }

    private void playPrevious(){
        AudioFile previousAudioFile = fragmentList.getPreviousAudio(selectedAudio);
        changeSelectedAudio = true;
        try {
            mediaPlayerService.play(previousAudioFile.getFilePath());
            selectedAudio = previousAudioFile;
            audioListener.onDoneSomething(selectedAudio);

        }
        catch (IOException e){
            System.out.println(e);
        }
    }

    private void playSelected(String path){
        changeSelectedAudio = false;
        try {
            mediaPlayerService.play(path);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void actionManager(String action){
        switch (clickedButton){
            case "init":{
                switch (action) {
                    case "play": {
                        playSelected(selectedAudio.getFilePath());
                    }
                    case "next": {
                        if (selectedAudio.getClass().getName() == "AudioFile"){
                            playNext();}

                        break;
                    }
                    case "previous": {
                        if (selectedAudio.getClass().getName() == "AudioFile"){
                            playPrevious();
                        }
                        break;
                    }
                    default: {
                        System.out.println("ERROR !!!   Action unknown : " + String.valueOf(action));
                        break;
                    }
                }
                break;
            }
            case "play": {
                switch (action) {
                    case "play": {
                        if (changeSelectedAudio) {
                            playSelected(selectedAudio.getFilePath());
                        }
                        break;
                    }
                    case "pause": {
                        mediaPlayerService.pause();
                        break;
                    }
                    case "next": {
                        playNext();
                        break;
                    }
                    case "previous": {
                        playPrevious();
                        break;
                    }
                    case "stop": {
                        mediaPlayerService.stop();
                        break;
                    }
                    default: {
                        System.out.println("ERROR !!!   Action unknown : " + String.valueOf(action));
                        break;
                    }
                }
                break;
            }
            case "pause": {
                switch (action) {
                    case "play": {
                        if (changeSelectedAudio) {
                            playSelected(selectedAudio.getFilePath());
                        }
                        else{
                            mediaPlayerService.playAfterPause();
                            break;
                        }
                    }
                    case "pause": {
                        break;
                    }
                    case "next": {
                        playNext();
                        break;
                    }
                    case "previous": {
                        playPrevious();
                        break;
                    }
                    case "stop": {
                        mediaPlayerService.stop();
                        break;

                    }
                    default: {
                        System.out.println("ERROR !!!   Action unknown : " + String.valueOf(action));
                        break;
                    }
                }
            }
            default: {
                switch (action) {
                    case "play": {
                        if (changeSelectedAudio) {
                            playSelected(selectedAudio.getFilePath());
                        }
                        break;
                    }
                    case "pause": {
                        mediaPlayerService.pause();
                        break;
                    }
                    case "next": {
                        playNext();
                        break;
                    }
                    case "previous": {
                        playPrevious();
                        break;
                    }
                    case "stop": {
                        mediaPlayerService.stop();
                        break;
                    }
                    default: {
                        System.out.println("ERROR !!!   Action unknown : " + String.valueOf(action));
                        break;
                    }
                }
                break;
            }
        }
        clickedButton = action;

    }



}

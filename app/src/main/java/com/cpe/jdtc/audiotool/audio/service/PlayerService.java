package com.cpe.jdtc.audiotool.audio.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.widget.SeekBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cpe.jdtc.audiotool.R;

import java.io.IOException;

public class PlayerService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private final Binder binder = new PlayerBinder();
    private MediaPlayer mediaPlayer = new MediaPlayer();
    public int onStartCommand(Intent intent, int flags, int startId) {

        //this.onPrepared(this.mediaPlayer);

        //String path = "/fs03/share/users/thibaut.chataing/home/Musique/Chad_Crouch_-_01_-_Headwaters_Instrumental.mp3";

        return START_STICKY_COMPATIBILITY;
    }

    public void play(String path) throws IOException {
        mediaPlayer.reset();
        mediaPlayer.setDataSource(path);
        mediaPlayer.prepareAsync();
    }

    public void playAfterPause(){
        mediaPlayer.start();
    }

    public  void pause(){
        mediaPlayer.pause();

    }

    public void next(){

    }

    public void previous(){

    }

    public void stop() {
        mediaPlayer.stop();
    }

    public boolean isPlaying(){
        return mediaPlayer.isPlaying();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setLooping(true);
        return binder;
    }

    @Override
    public void onPrepared(MediaPlayer player) {
        mediaPlayer.start();

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    public class PlayerBinder extends Binder {


        public PlayerService getService() {
            return PlayerService.this;
        }
    }

}

package com.cpe.jdtc.audiotool.audio.viewmodel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.cpe.jdtc.audiotool.audio.model.AudioFile;

public class ShortRunnerViewModel extends BaseObservable {

    private AudioFile currentAudioFile = new AudioFile();

    public void setAudioFile(AudioFile file) {
        currentAudioFile = file;
        notifyChange();
    }

    @Bindable
    public String getArtist() {
        return currentAudioFile.getArtist();
    }

    @Bindable
    public String getTitle() {
        return currentAudioFile.getTitle();
    }

    @Bindable
    public String getAlbum() {
        return currentAudioFile.getAlbum();
    }

    @Bindable
    public String getDuration() {
        return currentAudioFile.getDurationText();
    }

    public String getInformation() {
        return currentAudioFile.getInformation();
    }


    @Bindable
    public  AudioFile getAudioFile() { return  currentAudioFile; }
}
